﻿using System;
using System.Diagnostics;
using System.IO;
using System.Configuration;
using Otus.Teaching.Concurrency.Import.Core.Loaders;
using Otus.Teaching.Concurrency.Import.DataGenerator.Generators;
using Otus.Teaching.Concurrency.Import.DataAccess.Parsers;

namespace Otus.Teaching.Concurrency.Import.Loader
{
    class Program
    {
        private static string _dataFileDirectory = AppDomain.CurrentDomain.BaseDirectory;
        private static string _dataFileName = "customers";
        private static string _generatorFilePath = String.Empty;
        private static TypeOfLaunch _typeOfLaunch = TypeOfLaunch.MethodCall;
        private static int _dataCount = 10;
        private static int _threadsCount = 5;
        private static int _attemptNumber = 1;

        static void Main(string[] args)
        {
            SetSettings(args);

            if (_typeOfLaunch == TypeOfLaunch.MethodCall)
            {
                GenerateCustomersDataFile();
            }
            else
            {
                RunGeneratorCustomersDataFile();
            }

            XmlParser xmlParser = new XmlParser(Path.Combine(_dataFileDirectory, $"{_dataFileName}.xml"));
            
            XmlDataLoader loaderA = new XmlDataLoader(xmlParser.Parse(), _threadsCount, _attemptNumber, usingThreadPool : false);
            loaderA.LoadData();

            XmlDataLoader loaderB = new XmlDataLoader(xmlParser.Parse(), _threadsCount, _attemptNumber, usingThreadPool: true);
            loaderB.LoadData();

            Console.ReadLine();
        }

        private static void SetSettings(string[] args)
        {
            ValidateArgs(args);

            string setting = ReadSetting("TypeOfLaunch");
            if (setting != String.Empty)
            {
                if (Enum.IsDefined(typeof(TypeOfLaunch), setting))
                    _typeOfLaunch = Enum.Parse<TypeOfLaunch>(setting);
            }

            setting = ReadSetting("DataCount");
            if (setting != String.Empty)
            {
                int number;
                if (int.TryParse(setting, out number))
                    _dataCount = number;
            }

            setting = ReadSetting("ThreadsCount");
            if (setting != String.Empty)
            {
                int number;
                if (int.TryParse(setting, out number))
                    _threadsCount = number;
            }

            setting = ReadSetting("AttemptNumber");
            if (setting != String.Empty)
            {
                int number;
                if (int.TryParse(setting, out number))
                    _attemptNumber = number;
            }
        }

        private static void ValidateArgs(string[] args)
        {
            if (args != null && args.Length > 0)
                _generatorFilePath = args[0];
        }

        private static string ReadSetting(string key)
        {
            string result = String.Empty;
            try
            {
                result = ConfigurationManager.AppSettings[key] ?? String.Empty;                
            }
            catch (ConfigurationErrorsException)
            {
                return String.Empty;
            }

            return result;
        }
         
        static void GenerateCustomersDataFile()
        {
            var xmlGenerator = new XmlGenerator(Path.Combine(_dataFileDirectory, $"{_dataFileName}.xml"), _dataCount);
            xmlGenerator.Generate();
        }

        private static void RunGeneratorCustomersDataFile()
        {
            if (_generatorFilePath != String.Empty && File.Exists(_generatorFilePath))
            {
                ProcessStartInfo startInfo = new ProcessStartInfo()
                {
                    Arguments = $"{_dataFileName} {_dataCount} {_dataFileDirectory}",
                    FileName = _generatorFilePath
                };

                Process process = Process.Start(startInfo);
                process.WaitForExit();
            }
            else
                Console.WriteLine("Data generator file does not exist.");
        }

        private enum TypeOfLaunch
        {
            MethodCall, ProcessStart
        }
    }
}