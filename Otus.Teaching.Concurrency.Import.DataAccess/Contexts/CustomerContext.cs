﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Data.Entity;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Contexts
{
    public class CustomerContext : DbContext
    {
        public CustomerContext() : base("DbConnection")
        {}
        public DbSet<Customer> Customers { get; set; }
    }
}
