﻿using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    internal class ParameterizedThread
    {
        public ThreadParameters ThreadParameters { get; set; }
        public ParameterizedThread(ThreadParameters threadParameters)
        {
            ThreadParameters = threadParameters;            
        }
    }
}
