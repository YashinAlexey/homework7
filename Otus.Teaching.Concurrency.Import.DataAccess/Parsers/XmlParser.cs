﻿using System.Collections.Generic;
using System.IO;
using System.Xml.Serialization;
using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class XmlParser : IDataParser<CustomersList>
    {
        private readonly string _dataFile;

        public XmlParser(string dataFile)
        {
            _dataFile = dataFile;
        }

        public CustomersList Parse()
        {            
            using (FileStream _stream = new FileStream(_dataFile, FileMode.Open))
            {
                XmlSerializer xmlSerializer = new XmlSerializer(typeof(CustomersList));
                return (CustomersList)xmlSerializer.Deserialize(_stream);
            }                
        }
    }
}