﻿using Otus.Teaching.Concurrency.Import.Handler.Entities;
using System.Collections.Generic;
using System.Threading;

namespace Otus.Teaching.Concurrency.Import.Loader.Loaders
{
    internal class ThreadParameters
    {
        public int AttemptNumber { get; set; }
        public bool Successfully { get; set; }
        public List<Customer> Customers { get; set; }
        public AutoResetEvent WaitHandler { get; set; }
        public AutoResetEvent RerunHandler { get; set; }
        public ThreadParameters(List<Customer> customers, int attemptNumber)
        {
            AttemptNumber = attemptNumber;
            Successfully = true;
            Customers = customers;
            WaitHandler = new AutoResetEvent(true);
            RerunHandler = new AutoResetEvent(true);
        }
    }
}
