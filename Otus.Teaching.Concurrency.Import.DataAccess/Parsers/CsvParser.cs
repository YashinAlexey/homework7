﻿using Otus.Teaching.Concurrency.Import.Core.Parsers;
using Otus.Teaching.Concurrency.Import.DataGenerator.Dto;
using CsvHelper;
using System.IO;
using CsvHelper.Configuration;
using System.Globalization;
using Otus.Teaching.Concurrency.Import.Handler.Entities;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Parsers
{
    public class CsvParser : IDataParser<CustomersList>
    {
        private readonly string _dataFile;

        public CsvParser(string dataFile)
        {
            _dataFile = dataFile;
        }

        public CustomersList Parse()
        {
            CsvConfiguration configuration = new CsvConfiguration(CultureInfo.CurrentCulture);
            configuration.HasHeaderRecord = false;
            configuration.Delimiter = ";";

            using (StreamReader streamReader = new StreamReader(_dataFile))
            {
                using (CsvReader csvReader = new CsvReader(streamReader, configuration))
                {
                    return (CustomersList)csvReader.GetRecords<CustomersList>();
                }               
            }
        }
    }
}
