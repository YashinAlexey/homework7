using System;
using System.Collections.Generic;
using Otus.Teaching.Concurrency.Import.DataAccess.Contexts;
using Otus.Teaching.Concurrency.Import.Handler.Entities;
using Otus.Teaching.Concurrency.Import.Handler.Repositories;

namespace Otus.Teaching.Concurrency.Import.DataAccess.Repositories
{
    public class CustomerRepository : ICustomerRepository
    {
        public void AddCustomers(List<Customer> customers)
        {
            using (CustomerContext context = new CustomerContext())
            {
                context.Customers.AddRange(customers);
                context.SaveChanges();
            }
        }
    }
}